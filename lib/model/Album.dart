class Album {

  final String email;
  final int number;

  const Album({
    required this.email,
    required this.number,
  });

  factory Album.fromJson(Map<String, dynamic> json) {
    return Album(
      email: json['email'] as String,
      number: json['number'] as int,
    );
  }
}