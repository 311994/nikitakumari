import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'model/Album.dart';

Future<List<Album>> fetchAlbum() async {
  final response = await http
      .get(Uri.parse('http://192.168.1.36:8080/list'));

  if (response.statusCode == 200) {
    return compute(parseData, response.body);

  } else {
    throw Exception('Failed to load album');
  }
}
List<Album> parseData(String responseBody) {
  final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<Album>((json) => Album.fromJson(json)).toList();
}

// This class i have used for fetching firebase data using REST API I have create APi in python and using HTTP package i have
//integreate REST API In Flutter

class FetchDatausingAPI extends StatefulWidget {
  const FetchDatausingAPI({Key? key}) : super(key: key);

  @override
  _FetchDatausingAPIState createState() => _FetchDatausingAPIState();
}

class _FetchDatausingAPIState extends State<FetchDatausingAPI> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("FetchDataUsingAPI"),
      ),
      body: FutureBuilder<List<Album>>(
        future: fetchAlbum(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return const Center(
              child: Text('An error has occurred!'),
            );
          } else if (snapshot.hasData) {
            return DataList(albumdata: snapshot.data!);
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
class DataList extends StatelessWidget {
  const DataList({Key? key, required this.albumdata}) : super(key: key);

  final List<Album> albumdata;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: albumdata.length,
      itemBuilder: (context, index) {
        return Card(
            margin: EdgeInsets.all(10),
            child: ListTile(
            title: Text(albumdata[index].email),
        subtitle: Text(albumdata[index].number.toString()),
            ),
        );
      },
    );
  }
}
